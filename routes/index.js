var express = require('express');
var router = express.Router();

/* GET home page. *

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
*/
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Introduction', nav_id: "",header_nav_id: "home"});
});
router.get('/index', function(req, res, next) {
  res.render('index', { title: 'Introduction', nav_id: "",header_nav_id: "home"});
});
router.get('/about',function(req,res){
  res.render('about',{ title: 'About',nav_id: "","header_nav_id": "about"})
})
router.get('/delhi-intro', function(req, res, next) {
  res.render('2-delhi-intro', { title: 'Intro to Delhi Sultanate', nav_id: "nav1",header_nav_id: ""});
});
router.get('/invaders', function(req, res, next) {
  res.render('3-invaders', { title: 'Invaders', nav_id: "nav2",header_nav_id: ""});
});
router.get('/mamluk', function(req, res, next) {
  res.render('4-mamluk', { title: 'Mamluk', nav_id: "nav3",header_nav_id: ""});
});
router.get('/iltutmish', function(req, res, next) {
  res.render('5-iltutmish', { title: 'Mamluk', nav_id: "nav4",header_nav_id: ""});
});



module.exports = router;
